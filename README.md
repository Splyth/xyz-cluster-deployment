# XYZ-Cluster-Deployment
This repository deploys a EKS cluster into `us-east-2` named `gitlab-terraform-eks`

# Prerequistes
1. An AWS Account with permissions to create an EKS Cluster
2. A Gitlab Account
3. [Register Gitlab Agent](https://about.gitlab.com/blog/2022/11/15/simple-kubernetes-management-with-gitlab/#register-the-gitlab-agent)
3. [Set CI/CD Variables](https://about.gitlab.com/blog/2022/11/15/simple-kubernetes-management-with-gitlab/#aws-eks)

# Usage
These terraform files will create a cluster using terraform and the GitLab CI/CD Pipeline.
This project is based off the EKS CI/CD Template found here: https://gitlab.com/gitlab-org/configure/examples/gitlab-terraform-eks

Shamelessly stolen from this guide: https://about.gitlab.com/blog/2022/11/15/simple-kubernetes-management-with-gitlab/

1. Make changes
2. Check in to Gitlab Repo
3. Pipeline will kick off and validate and test the changes
4. Pipeline will NOT automatically deploy to EKS (requires you to go into the pipeline and press go)
   a. You do NOT need supply it with a variable

# New Project Setup

1. [Register New Gitlab Agent](https://docs.gitlab.com/ee/user/clusters/agent/install/)
2. Open your Gitlab Repo's Settings -> CI/CD
3. Expand Variables
4. Add Variable: `AWS_ACESS_KEY` Value: `Your AWS Account's AWS ACCESS KEY`
5. Add Variable: `AWS_SECRET_ACESS_KEY` Value: `Your AWS Account's AWS SECRET ACCESS KEY`
6. Add Variable: `TF_VAR_agent_token` Value: `Your Gitlab Agent Token`
7. Add Variable: `TF_VAR_kas_address` Value: `Your Gitlab Agent Token Key Access Address`
